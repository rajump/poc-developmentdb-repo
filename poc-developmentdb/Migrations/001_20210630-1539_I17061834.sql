﻿-- <Migration ID="317726cf-6409-4d62-8bc9-e0c9a582f942" />
GO

PRINT N'Creating [dbo].[DEPT]'
GO
CREATE TABLE [dbo].[DEPT]
(
[deptno] [int] NOT NULL,
[dname] [varchar] (14) NULL,
[loc] [varchar] (13) NULL
)
GO
PRINT N'Creating [dbo].[EMP]'
GO
CREATE TABLE [dbo].[EMP]
(
[empno] [int] NOT NULL,
[ename] [varchar] (10) NULL,
[job] [varchar] (9) NULL,
[mgr] [int] NULL,
[hiredate] [datetime] NULL,
[sal] [numeric] (7, 2) NULL,
[comm] [numeric] (7, 2) NULL,
[dept] [int] NULL
)
GO
PRINT N'Creating primary key [PK__EMP__AF4C318A744F9286] on [dbo].[EMP]'
GO
ALTER TABLE [dbo].[EMP] ADD CONSTRAINT [PK__EMP__AF4C318A744F9286] PRIMARY KEY CLUSTERED ([empno])
GO
PRINT N'Creating [dbo].[TEST_LOB]'
GO
CREATE TABLE [dbo].[TEST_LOB]
(
[OTC_CD] [varchar] (6) NULL,
[OTC_HQ_CD] [varchar] (6) NULL,
[RCPMNT_FILE] [varchar] (max) NULL
)
GO
